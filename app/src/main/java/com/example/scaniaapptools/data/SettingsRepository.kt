package com.example.scaniaapptools.data

import kotlinx.coroutines.flow.Flow

class SettingsRepository(private val settingsLocalData: SettingsLocalData) {

    fun loadData(): Flow<SettingsData> {
        return settingsLocalData.loadData()
    }

    fun loadRawData(): Flow<String> {
        return settingsLocalData.loadRawData()
    }

    fun setPhoneSettingsEnabled(enabled: Boolean): Flow<SettingsData> {
       return settingsLocalData.setEnablePhoneSettings(enabled)
    }
}