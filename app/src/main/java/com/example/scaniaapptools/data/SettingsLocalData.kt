package com.example.scaniaapptools.data

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

private const val EMPTY_JSON = "{}"

class SettingsLocalData(private val dataStore: DataStore<Preferences>, private val moshi: Moshi) {
    private val settings = stringPreferencesKey("settings")
    private val adapter: JsonAdapter<SettingsData> = moshi.adapter(SettingsData::class.java)

    fun loadData(): Flow<SettingsData> {

        return dataStore.data.map {
            it.toObject()
        }
    }
    fun loadRawData(): Flow<String> {

        return dataStore.data.map {
            return@map it[settings] ?: EMPTY_JSON
        }
    }

    fun setEnablePhoneSettings(enabled: Boolean): Flow<SettingsData> {
        return flow {
            dataStore.edit {
                val settingsData = it.toObject()
                val newSettingsData = settingsData.copy(enablePhoneSettings = enabled)
                it[settings] = adapter.toJson(newSettingsData)
                emit(newSettingsData)
            }

        }

    }

    private fun Preferences.toObject(): SettingsData {
        val rawData = this[settings] ?: EMPTY_JSON
        return adapter.fromJson(rawData) ?: SettingsData()
    }

}