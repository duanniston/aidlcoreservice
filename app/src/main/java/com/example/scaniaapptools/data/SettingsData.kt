package com.example.scaniaapptools.data

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class SettingsData(@Json(name = "enablePhoneSettings") val enablePhoneSettings: Boolean = false) :
    Parcelable