package com.example.scaniaapptools.domain

import com.example.scaniaapptools.data.SettingsData
import com.example.scaniaapptools.data.SettingsRepository
import kotlinx.coroutines.flow.Flow

class SettingsUseCase(private val settingsRepository: SettingsRepository) {

    fun loadData(): Flow<SettingsData> {
        return settingsRepository.loadData()
    }

    fun loadRawData(): Flow<String> {
        return settingsRepository.loadRawData()
    }

    fun setPhoneSettingsEnabled(enabled: Boolean): Flow<SettingsData> {
        return settingsRepository.setPhoneSettingsEnabled(enabled)
    }
}