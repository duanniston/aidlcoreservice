package com.example.scaniaapptools.application

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.example.scaniaapptools.AIDLSettings
import com.example.scaniaapptools.BuildConfig
import com.example.scaniaapptools.domain.SettingsUseCase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import timber.log.Timber

class SettingsService : Service() {

    private val bind: SettingsBind = SettingsBind()

    override fun onBind(intent: Intent?): IBinder {
        Timber.d("onBind: $intent")
        return bind
    }

    class SettingsBind : AIDLSettings.Stub(), KoinComponent {

        private val settingsUseCase by inject<SettingsUseCase>()

        override fun getSettings(): String {
            Timber.d("getSettings")
            return runBlocking {
                settingsUseCase.loadRawData().first()
            }
        }

        override fun getSecretCode(): Int {
            Timber.d("getSecretCode")
            return BuildConfig.SECRET_CODE
        }

    }
}