package com.example.scaniaapptools.application

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.preferencesDataStore
import com.example.scaniaapptools.data.SettingsLocalData
import com.example.scaniaapptools.data.SettingsRepository
import com.example.scaniaapptools.domain.SettingsUseCase
import com.squareup.moshi.Moshi
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private const val PREFERENCE_NAME = "settings"

object AppModule {
    val module = module {

        single { SettingsRepository(settingsLocalData = get()) }
        factory { SettingsUseCase(settingsRepository = get()) }
        single { SettingsLocalData(dataStore = get(), moshi = get()) }
        single { androidContext().dataStore }
        single { Moshi.Builder().build() }
        viewModel { SettingsViewModel(application = androidApplication(), settingsUseCase = get()) }

    }
    private val Context.dataStore: DataStore<androidx.datastore.preferences.core.Preferences> by preferencesDataStore(
        name = PREFERENCE_NAME
    )
}
