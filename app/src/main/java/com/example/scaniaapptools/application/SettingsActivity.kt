package com.example.scaniaapptools.application

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import com.example.scaniaapptools.AIDLSettings
import com.example.scaniaapptools.databinding.MainActivityBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class SettingsActivity : AppCompatActivity() {

    private lateinit var viewBinding: MainActivityBinding
    val viewModel: SettingsViewModel by viewModel()
    private var aidlSettings: AIDLSettings? = null
    private var serviceConnection: ServiceConnection? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = MainActivityBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        setView()
        setData()
        serviceConnection = object : ServiceConnection {
            override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
                aidlSettings = AIDLSettings.Stub.asInterface(p1)
                Timber.d("onServiceConnected: ${aidlSettings?.secretCode} -- $p0")
            }

            override fun onServiceDisconnected(p0: ComponentName?) {
                Timber.d("onServiceDisconnected: $p0")
            }
        }


    }

    private fun setData() {
        viewModel.settingsDataLiveData.observe(this) {
            Timber.i("settingsDataLiveData: $it")
            viewBinding.switchPhoneSettings.isChecked = it.enablePhoneSettings
        }
        viewModel.loadData()

    }

    private fun setView() {
        viewBinding.switchPhoneSettings.setOnCheckedChangeListener { buttonView, isChecked ->
            viewModel.setPhoneSettingsEnabled(isChecked)
        }

    }
}