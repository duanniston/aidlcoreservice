package com.example.scaniaapptools.application

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.scaniaapptools.data.SettingsData
import com.example.scaniaapptools.domain.SettingsUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import timber.log.Timber

class SettingsViewModel(application: Application, private val settingsUseCase: SettingsUseCase) :
    AndroidViewModel(application) {

    val settingsDataLiveData: MutableLiveData<SettingsData> = MutableLiveData()

    fun loadData() {
        settingsUseCase.loadData().catch {
            Timber.e(it)
        }.onEach {
            Timber.d("loadData: $it")
            settingsDataLiveData.postValue(it)
        }.flowOn(Dispatchers.IO).launchIn(viewModelScope)
    }

    fun setPhoneSettingsEnabled(enabled: Boolean) {
        settingsUseCase.setPhoneSettingsEnabled(enabled).catch {
            Timber.e(it)
        }.onEach {
            Timber.d("setPhoneSettingsEnabled: $it")
        }.launchIn(viewModelScope)

    }
}